.. Bless Online documentation master file, created by
   sphinx-quickstart on Tue Apr 24 22:13:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Bless Online documentation!
======================================================

You will be able to find some some guides, informations about Bless Online Related things. 
The main objective of the project is to provide an easy to navigate, responsive site
that contains gameplay centered articles in an easy to read/understand format.
So you won't find long winded guides here only sweet and short ones, we don't want to
waste your time. 

.. toctree::
   :maxdepth: 1
   :caption: Guides
   :name: sep-Guides

   guides/dungeons/index

.. toctree::
   :maxdepth: 1
   :caption: Community
   :name: sep-Community

   community/how_to_contribute_to_the_docs/index
   community/extreme_guild_specific_content/index