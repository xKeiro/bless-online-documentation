Extreme guild specific content
===============================

This section is dedicated to the “eXtreme” guild. It contains guides/information 
that otherwise wouldn’t fit the Bless Docs. And they are generally a lot less moderated.

.. toctree::
   :maxdepth: 1
   :name: toc-community-extreme_guild_specific_content

   x_about_guilds